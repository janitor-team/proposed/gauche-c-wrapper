gauche-c-wrapper (0.6.1-12) unstable; urgency=medium

  * debian/patches/11_fix_jp_encoding.patch: New.  From Arch.
  * debian/patches/12_float128.patch: New.  From Arch.
  * debian/patches/13_local_typedef.patch: New.  From Arch.
  * debian/patches/14_extend_parser.patch: New.  From Arch.
    Fixed FTBFS (Closes: #925691).
    Update removing trailing space.
    Update for API change of Scm_RegExec in Gauche 0.9.10.
    Update to include generated files.
  * debian/control (Standards-Version): Conforms to 4.5.1.
    (Build-Depends): Add debhelper-compat.  Add bison.
  * debian/compat: Remove.
  * debian/rules (override_dh_auto_configure): Touch c-parser.c.

 -- NIIBE Yutaka <gniibe@fsij.org>  Fri, 15 Jan 2021 09:50:22 +0900

gauche-c-wrapper (0.6.1-11) unstable; urgency=medium

  * Fix FTBFS (Closes: #926788).
  * debian/patches/10-fix-closure-alloc.patch: New.

 -- NIIBE Yutaka <gniibe@fsij.org>  Tue, 16 Apr 2019 12:24:07 +0900

gauche-c-wrapper (0.6.1-10) unstable; urgency=medium

  * debian/patches/07_gzip_options.patch: New.
  * debian/patches/08-gen-gpd-fix.patch: New.
  * debian/patches/09-cpp-c-ldflags.patch: New.

 -- NIIBE Yutaka <gniibe@fsij.org>  Mon, 22 Oct 2018 09:10:28 +0900

gauche-c-wrapper (0.6.1-9) unstable; urgency=medium

  * debian/control (Standards-Version): Conforms to 4.2.1.
  (Build-Depends): This is for new gauche-dev (>= 0.9.6).
  (Maintainer): Don't use mailing list (Closes: #899522).
  * debian/compat: Upgrade to 11.
  * debian/patches/06___glibc_macro_warning.patch: New.
  * debian/rules: Update.
  * New package lets build amd64 package (Closes: #790036).

 -- NIIBE Yutaka <gniibe@fsij.org>  Tue, 09 Oct 2018 10:39:08 +0900

gauche-c-wrapper (0.6.1-8) unstable; urgency=medium

  * debian/control (Standards-Version): Conforms to 3.9.8.
  (Build-Depends): This is for new gauche-dev (>= 0.9.5).
  (Depends): Add gauche-dev.  Add gcc (Closes: #822250)
  * debian/patches/series: Update.
  * debian/patches/05_cflags.patch: New.

 -- NIIBE Yutaka <gniibe@fsij.org>  Wed, 26 Oct 2016 10:32:39 +0900

gauche-c-wrapper (0.6.1-7) unstable; urgency=low

  [ Jens Thiele ]
  * debain/patches/04_build_with_gcc5.patch: Update.

  [ NIIBE Yutaka ]
  * debian/control: Add Vcs-Git and Vcs-Browser fields.

 -- NIIBE Yutaka <gniibe@fsij.org>  Mon, 21 Dec 2015 22:00:39 +0900

gauche-c-wrapper (0.6.1-6) unstable; urgency=low

  [ Jens Thiele ]
  * debain/patches/04_build_with_gcc5.patch: New.
    Thanks to Matthias Klose and Martin Michlmayr.  Closes: #777861.

  [ NIIBE Yutaka ]
  * debian/control (Standards-Version): Conforms to 3.9.6.
    (Maintainer): Remove Hatta Shuzo.
  * debian/rules (GZIP): Add -n for reproducibility.

 -- NIIBE Yutaka <gniibe@fsij.org>  Sat, 19 Dec 2015 03:03:33 +0000

gauche-c-wrapper (0.6.1-5) unstable; urgency=low

  * debian/rules (DH_OPTIONS): Added.
    (override_dh_auto_configure): Don't call dh_auto_configure, but call
    configure by ourselves (because of multiarch badness).
  * debian/compat: Upgrade to 9.
  * debian/control (Standards-Version): Now, it follows 3.9.3.
    (Maintainer): Now, it's team maintained.
    (Uploaders): New (same as gauche).
    (Homepage): Update to new homepage.
  * debian/watch: Update.

 -- NIIBE Yutaka <gniibe@fsij.org>  Fri, 25 Jan 2013 14:29:32 +0900

gauche-c-wrapper (0.6.1-4.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "FTBFS: gcc: error: /usr/lib/libffi_pic.a: No such file or
    directory": apply patch from Ubuntu / Matthias Klose, changing
    01_use_installed_libffi.patch:
    - Search libffi_pic.a in the multiarch location.
      Closes: #634394. LP: #831289.
  * Explicitly build-depend on dpkg-dev (>= 1.16.0) to get dpkg-architecture's
    DEB_HOST_MULTIARCH variable.

 -- gregor herrmann <gregoa@debian.org>  Sun, 09 Oct 2011 14:53:27 +0200

gauche-c-wrapper (0.6.1-4) unstable; urgency=low

  * Build with new gauche 0.9-15.
  * debian/control (Depends): Added gauche.

 -- NIIBE Yutaka <gniibe@fsij.org>  Mon, 10 May 2010 10:49:54 +0900

gauche-c-wrapper (0.6.1-3) unstable; urgency=low

  * debian/patches/03_target_gnu.patch: Renamed from 03_gnu_host.patch.
  * debian/patches/03_target_gnu.patch: Move kfreebsd case before freebsd.
  * debian/patches/02_multilib.patch: Care for i[567]86-linux-gnu.

 -- NIIBE Yutaka <gniibe@fsij.org>  Wed, 07 Apr 2010 10:00:32 +0900

gauche-c-wrapper (0.6.1-2) unstable; urgency=low

  * debian/patches/03_gnu_host.patch: New file.
  * debian/patches/02_multilib.patch: Care for sparc*linux-gnu.

 -- NIIBE Yutaka <gniibe@fsij.org>  Fri, 02 Apr 2010 13:57:01 +0900

gauche-c-wrapper (0.6.1-1) unstable; urgency=low

  * debian/source.lintian-overrides: New file.
  * debian/patches/00_info_no_install.patch: New file.
  * debian/patches/01_use_installed_libffi.patch: New file from diff of
    0.5.4-2.
  * debian/patches/02_multilib.patch: New file.
  * debian/patches/series: New file.
  * debian/gauche-c-wrapper.info: Append "gz" suffix.
  * debian/{rules,source,README.source}: Use 3.0 (quilt) format.
  * debian/compat: Updated.
  * debian/control (Section): It's now lisp.
    (Homepage): New field.
    (Depends): Added ${misc:Depends}.
    (Build-Depends): This is for new gauche-dev (>= 0.9).
    Added autoconf.
  * New upstream.

 -- NIIBE Yutaka <gniibe@fsij.org>  Fri, 19 Mar 2010 22:15:02 +0900

gauche-c-wrapper (0.5.4-2) unstable; urgency=low

  * debian/rules: Supply LDCONFIG for configure (Closes: #490717).
  Thanks to Jens Thiele.

  NOTE: This workaround should be removed when upstream incorporates
  fix to configure.ac.

 -- NIIBE Yutaka <gniibe@fsij.org>  Wed, 16 Jul 2008 10:51:00 +0900

gauche-c-wrapper (0.5.4-1) unstable; urgency=low

  * New upstream release (DESTDIR change and _pic.a change has been
    incorporated into upstream).
  * debian/control (Build-Depends): Incorporate NMU change of 0.5.2-5.1.
  (Standards-Version): Conform to 3.8.0 (was: 3.7.3).
  * debian/watch: Fixed.

 -- NIIBE Yutaka <gniibe@fsij.org>  Fri, 04 Jul 2008 09:21:12 +0900

gauche-c-wrapper (0.5.2-5.1) unstable; urgency=low

  * Non-maintainer upload.
  * Build depend on libffi-dev instead of libffi4-dev. Closes: #479722.

 -- Frans Pop <fjp@debian.org>  Fri, 09 May 2008 23:21:28 +0200

gauche-c-wrapper (0.5.2-5) unstable; urgency=low

  * debian/gauche-c-wrapper.examples: New file (Closes: #470677).
  Thanks to Jens Thiele.

 -- NIIBE Yutaka <gniibe@fsij.org>  Mon, 07 Apr 2008 09:15:03 +0900

gauche-c-wrapper (0.5.2-4) unstable; urgency=low

  * src/Makefile.in: Use libffi_pic.a instead of libffi.a.
  * debian/rules (clean): Remove config.sub and config.guess.
  * debian/control (Build-Depends): Added autotools-dev.

 -- NIIBE Yutaka <gniibe@fsij.org>  Fri, 29 Feb 2008 16:07:29 +0900

gauche-c-wrapper (0.5.2-3) unstable; urgency=low

  * src/Makefile.in: Use installed libffi.a.
  * debian/control (Build-Depends): Added libffi4-dev.

 -- NIIBE Yutaka <gniibe@fsij.org>  Thu, 28 Feb 2008 21:24:36 +0900

gauche-c-wrapper (0.5.2-2) unstable; urgency=low

  * debian/rules: Removed LDFLAGS="-Wl,-z,defs".

 -- NIIBE Yutaka <gniibe@fsij.org>  Tue, 19 Feb 2008 13:04:47 +0900

gauche-c-wrapper (0.5.2-1) unstable; urgency=low

  * Initial release (Closes: #462492).

 -- NIIBE Yutaka <gniibe@fsij.org>  Mon, 18 Feb 2008 14:46:17 +0900
